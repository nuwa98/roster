# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.38)
# Database: roster
# Generation Time: 2015-06-28 13:31:24 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_profiles`;

CREATE TABLE `tbl_profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `nokp` varchar(12) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_profiles` WRITE;
/*!40000 ALTER TABLE `tbl_profiles` DISABLE KEYS */;

INSERT INTO `tbl_profiles` (`user_id`, `first_name`, `nokp`)
VALUES
	(1,'Administrator',''),
	(32,'Supervisor','999999999999'),
	(36,'Worker','234232424222'),
	(37,'Worker 2','234232424222'),
	(38,'Worker 3','230482390482'),
	(39,'Worker 4','342342328484'),
	(40,'worker 5','893749837493'),
	(41,'Worker 6','238947928374'),
	(42,'worker 7','239472394723'),
	(43,'worker 8','239742893479'),
	(44,'worker 9','234234223484'),
	(45,'woker 10','287983743434'),
	(46,'worker 11','298379397934'),
	(47,'worker 12','98323048911'),
	(48,'worker 13','239847329847'),
	(49,'worker 14','238947973434'),
	(50,'worker 15','238472398472'),
	(51,'worker 16','293472384723'),
	(52,'worker 17','234723984278'),
	(53,'worker 18','923798387439'),
	(54,'worker 19','234238948230'),
	(55,'worker 20','234923749238'),
	(56,'worker 21','234823749823'),
	(57,'worker 22','234897932423'),
	(58,'worker 23','234798273427'),
	(59,'Worker 24','293847897798'),
	(60,'worker 25','987982343242'),
	(61,'worker 26','987982343243'),
	(62,'worker 27','298739427948'),
	(63,'worker 28','657618903180'),
	(64,'worker 29','987987293473'),
	(65,'worker 30','302389472342'),
	(66,'worker 31','234234242342'),
	(67,'worker 32','234982739427'),
	(68,'worker 33','927983724234'),
	(69,'worker 34','234284923234'),
	(70,'worker 35','234234889080');

/*!40000 ALTER TABLE `tbl_profiles` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
