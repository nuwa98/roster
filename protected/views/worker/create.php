<?php
/* @var $this WorkerController */
/* @var $model TblUsers */

$this->breadcrumbs=array(
	'Worker'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TblUsers', 'url'=>array('index')),
	array('label'=>'Manage TblUsers', 'url'=>array('admin')),
);
?>

<div class="box box-solid box-default">
    <div class="box-header">
        <h3 class="box-title">Add New Worker</h3>
    </div>
    <div class="box-body">

        <div class="alert alert-info">

        	<strong>NOTE!</strong> IC Number will be a default password for worker
        </div>

        <?php $this->renderPartial('_form', array('model'=>$model, 'profile'=>$profile)); ?>
    </div>
</div>


