<?php
$this->pageTitle = Yii::app()->name.' - View Report';
/* @var $this WorkerController */
/* @var $model TblUsers */

$this->breadcrumbs=array(
    'Worker'=>array('index'),
    'View Report'
);
?>

<div class="row">
    <div class="col-lg-6 col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Select Month & Year</h3>
            </div>
            <div class="panel-body">
                <?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
                    'id'=>'shifts-form',
                    // Please note: When you enable ajax validation, make sure the corresponding
                    // controller action is handling ajax validation correctly.
                    // There is a call to performAjaxValidation() commented in generated controller code.
                    // See class documentation of CActiveForm for details on this.
                    'enableAjaxValidation'=>false,
                )); ?>

                <div class="form-group">
                    <label class="form-label" for="month">Month</label>
                    <div>
                        <?php echo CHtml::dropDownList('month', $month, BaseFunctions::monthList(), array('class'=>'form-control'));?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label" for="year">Year</label>
                    <div>
                        <?php echo CHtml::dropDownList('year', $year, BaseFunctions::yearList(), array('class'=>'form-control'));?>
                    </div>
                </div>

                <div class="form-actions">
                    <?php echo CHtml::submitButton('Set', array('class'=>'btn btn-success', 'name'=>'set_month')); ?>
                </div>

                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-sm-6">
        <div class="panel panel-info">
        	  <div class="panel-heading">
        			<h3 class="panel-title">Staff Information</h3>
        	  </div>
                <?php $this->widget('booster.widgets.TbDetailView', array(
                    'data'=>$worker,
                    'type'=>'',
                    'attributes'=>array(
                        'profile.first_name',
                        'profile.nokp',
                        'email',
                        'status',
                        'lastvisit_at',
                    ),
                )); ?>
        </div>
    </div>
</div><!-- end row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
        		<h3 class="panel-title">Monthly Report</h3>
        	</div>
            <div class="panel-body">
                <h3>Report For <?php echo BaseFunctions::monthDetails($month)?>, <?php echo $year;?></h3>
                <p><b>Total Working Hours : <?php echo $working_hours;?> hours</b></p>
                <p><b>Total Working Days : <?php echo $totalWorkingDays;?> days</b></p>
                <p><b>Total Off Day : <?php echo $totalOffDay;?> days</b></p>
            </div>
            <div class="table-responsive">
                <table class="table table-condensed table-hover">
                    <thead>
                        <tr>
                            <th style="width:30px;">NO.</th>
                            <th>Date</th>
                            <th>Shift</th>
                            <th>Shift Details</th>
                            <th>Zone</th>
                            <th>Total Hours</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $bil=1;?>
                        <?php foreach($find_schedule as $schedule) { ?>
                            <tr>
                                <td><?php echo $bil;?></td>
                                <td><?php echo date('d-M-Y', strtotime($schedule->date));?></td>
                                <td><?php echo (isset($schedule->shift->name))?$schedule->shift->name:"";?></td>
                                <td>
                                    <?php
                                    if(isset($schedule->shift)) {
                                        if($schedule->shift->off==0)
                                            echo date('h:i A', strtotime($schedule->shift->starttime)) . ' - '.date('h:i A', strtotime($schedule->shift->endtime));
                                        else
                                            echo "-";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php if($schedule->shift->off==0) { ?>
                                        <?php echo (isset($schedule->venue->name))?$schedule->venue->name:"Supervisor Not Update Yet";?></td>
                                    <?php } ?>
                                <td>
                                    <?php if($schedule->shift->off==0) { ?>
                                        7 working hours + 1 hour break
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php $bil++; ?>
                        <?php } ?>
                    </tbody>
                    <?php if(!$find_schedule) { ?>
                        <tfoot>
                            <tr>
                                <td colspan="6">No Data Found</td>
                            </tr>
                        </tfoot>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div><!-- end col -->
</div><!-- end row -->