<?php
/* @var $this WorkerController */
/* @var $model TblUsers */

$this->breadcrumbs=array(
	'Worker',
);


Yii::app()->clientScript->registerScript('search', "

    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });

    $('.search-form form').submit(function(){
        $('#tbl-users-grid').yiiGridView('update', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<p>
    <?php echo CHtml::link('<span class="glyphicon glyphicon-plus"></span> Add New Worker', array('create'), array('class'=>'btn btn-primary'));?>
</p>

<div class="box box-solid box-default">
    <div class="box-header">
        <h3 class="box-title">List of Worker</h3>
    </div>
    <div class="box-body no-padding">
        <?php $this->widget('booster.widgets.TbGridView', array(
            'id'=>'tbl-users-grid',
            'type'=>'bordered condensed',
            'ajaxUpdate'=>false,
            'dataProvider'=>$model->worker(),
//            'filter'=>$model,
            'columns'=>array(
                array(
                    'header'=>'#',
                    'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',       //  row is zero based
                    'htmlOptions'=>array('style'=>'width:30px;'),
                ),
                array(
                    'name'=>'first_name',
                    'value'=>'(isset($data->profile->first_name)) ? $data->profile->first_name:""',
                ),
                array(
                    'name'=>'nokp',
                    'value'=>'(isset($data->profile->nokp)) ? $data->profile->nokp:""',
                ),
                'username',
                'email',
                array(
                    'class'=>'CButtonColumn',
                    'template'=>'{report}',
                    'buttons'=>array(
                        'report'=>array(
                            'label'=>'Report',
                            'url'=>'Yii::app()->createUrl("worker/report",array("id"=>$data->id))',
                            'options'=>array('class'=>'btn btn-success btn-sm'),
                            'imageUrl'=>false,
                        ),
                    ),
                ),
                array(
                    'htmlOptions'=>array('nowrap'=>'nowrap'),
                    'class'=>'CButtonColumn',
                    'template'=>'{view} {update} {delete}',
                    'buttons'=>array(
                        'view'=>array(
                            'label'=>'View',
                            'options'=>array('class'=>'btn btn-primary btn-sm'),
                            'imageUrl'=>false,
                        ),
                        'update'=>array(
                            'label'=>'Update',
                            'options'=>array('class'=>'btn btn-warning btn-sm'),
                            'imageUrl'=>false,
                        ),
                        'delete'=>array(
                            'label'=>'Delete',
                            'options'=>array('class'=>'btn btn-danger btn-sm'),
                            'imageUrl'=>false,
                        ),
                    ),
                ),
            ),
        )); ?>
    </div>
</div>