<?php
/* @var $this WorkerController */
/* @var $model TblUsers */

$this->breadcrumbs=array(
	'Tbl Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TblUsers', 'url'=>array('index')),
	array('label'=>'Create TblUsers', 'url'=>array('create')),
	array('label'=>'Update TblUsers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TblUsers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TblUsers', 'url'=>array('admin')),
);
?>

<h1>View TblUsers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'password',
		'email',
		'activkey',
		'superuser',
		'status',
		'create_at',
		'lastvisit_at',
	),
)); ?>
