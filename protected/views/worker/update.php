<?php
/* @var $this WorkerController */
/* @var $model TblUsers */

$this->breadcrumbs=array(
	'Tbl Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TblUsers', 'url'=>array('index')),
	array('label'=>'Create TblUsers', 'url'=>array('create')),
	array('label'=>'View TblUsers', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TblUsers', 'url'=>array('admin')),
);
?>

<h1>Update TblUsers <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>