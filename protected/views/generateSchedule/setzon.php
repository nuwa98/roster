<?php
$this->pageTitle = Yii::app()->name. ' - Set Zone';
/* @var $this GenerateScheduleController */

$this->breadcrumbs=array(
    'Generate Schedule'=>array('index'),
    'Set Zone',
);
?>

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">Date Information</h3>
    </div>
    <div class="panel-body">
        <h3>Date : <?php echo date('d-m-Y', strtotime($date));?></h3>
    </div>
</div>

<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">List Schedule Details</h3>
    </div>

    <?php echo CHtml::beginForm();?>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="width: 40px;">No.</th>
                    <th>Name</th>
                    <th>Shift</th>
                    <th>Zone</th>
                </tr>
                </thead>
                <tbody>
                <?php $bil=1;?>
                <?php foreach($schedules as $key => $schedule) { ?>
                    <tr>
                        <td><?php echo $bil ?>. </td>
                        <td><?php echo $schedule->user->profile->first_name;?></td>
                        <td><?php echo $schedule->shift->name;?></td>
                        <td>
                            <?php echo CHtml::hiddenField("id[$key]", $schedule->id);?>
                            <?php echo CHtml::dropDownList("zone[$key]", $schedule->venue_id, CHtml::listData(Venue::model()->findAll(), 'id', 'name'), array("class"=>'form-control', 'empty'=>'- Please Select Zone -')); ?>
                        </td>
                    </tr>
                    <?php $bil++;?>
                <?php } ?>
                </tbody>
            </table>
        </div>

        <div class="form-actions pull-right" style="padding: 20px;">
            <?php echo CHtml::submitButton('Set Zone', array('class'=>'btn btn-success', 'name'=>'set_zone')); ?>
        </div>
        <div class="clearfix"></div>
    <?php echo Chtml::endForm();?>


</div>