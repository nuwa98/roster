<?php
$this->pageTitle = Yii::app()->name. ' - Generate Schedule';
/* @var $this GenerateScheduleController */

$this->breadcrumbs=array(
    'Generate Schedule',
);

?>


<div class="panel panel-info">
	  <div class="panel-heading">
			<h3 class="panel-title">Set Month & Year</h3>
	  </div>
	  <div class="panel-body">
          <?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
              'id'=>'shifts-form',
              // Please note: When you enable ajax validation, make sure the corresponding
              // controller action is handling ajax validation correctly.
              // There is a call to performAjaxValidation() commented in generated controller code.
              // See class documentation of CActiveForm for details on this.
              'enableAjaxValidation'=>false,
          )); ?>

            <div class="form-group">
                <label class="form-label" for="month">Month</label>
                <div>
                    <?php echo CHtml::dropDownList('month', $month, BaseFunctions::monthList(), array('class'=>'form-control'));?>
                </div>
            </div>
            <div class="form-group">
                <label class="form-label" for="year">Year</label>
                <div>
                    <?php echo CHtml::dropDownList('year', $year, BaseFunctions::yearList(), array('class'=>'form-control'));?>
                </div>
            </div>

          <div class="form-actions">
              <?php echo CHtml::submitButton('Set', array('class'=>'btn btn-success', 'name'=>'set_month')); ?>
          </div>

          <?php $this->endWidget(); ?>
	  </div>
</div>




<p>
    <?php echo CHtml::link('<i class="fa fa-calendar"></i> Generate Schedule For '.BaseFunctions::monthDetails($month). ', '.$year, array('generate'), array('class'=>'btn btn-primary'));?>
</p>
<div class="box box-solid box-default">

    <div class="box-header">
        <h3 class="box-title">
            Schedule for <?php echo BaseFunctions::monthDetails($month);?>, <?php echo $year?>
        </h3>
    </div><!-- end box-header -->

    <div class="box-body no-padding">

        <div style="padding: 10px;" class="well">
            <?php foreach($shifts as $shift){ ?>
                Shift <?php echo $shift->name?> : <?php echo date('h:i A', strtotime($shift->starttime))?> - <?php echo date('h:i A', strtotime($shift->endtime))?><br>
            <?php } ?>
        </div>

        <div style="padding: 20px;">
            <small>Click on each date number to set a ZON for each staff</small>
        </div>
        <table class="table table-condensed table-hover table-bordered">
        	<thead>
        		<tr>
        			<th>No</th>
                    <th>Employee Name</th>
                    <?php for($i=1; $i <= $total_day; $i++){ ?>
                        <th style="width: 30px;">
                            <a href="<?php echo Yii::app()->createUrl('generateSchedule/setzon', array('id'=>$i)); ?>"><?php echo $i;?></a>
                        </th>
                    <?php } ?>
        		</tr>
        	</thead>
        	<tbody>
                <?php $no = 1; ?>
                <?php foreach($workers as $worker){ ?>



                    <tr>
                        <td><?php echo $no ?></td>
                        <td><?php echo $worker->profile->first_name;?></td>
                        <?php for($i=1; $i <= $total_day; $i++){ ?>

                            <?php
                                // get data shift berpandukan worker id, and date
                                $date = $year.'-'.$month.'-'.$i;
                                $schedule = Schedules::model()->findByAttributes(array(
                                    'user_id'=>$worker->id,
                                    'date'=>$date,
                                ));
                            ?>

                            <?php
                            $background="";
                            if (isset($schedule->shift))
                            {
                                if($schedule->shift->id==4){
                                    $background="red";
                                }
                                else{
                                    if($schedule->shift->id==1){
                                        $background="green";
                                    }
                                    elseif($schedule->shift->id==2){
                                        $background="yellow";
                                    }
                                    elseif($schedule->shift->id==3){
                                        $background="blue";
                                    }
                                }
                            }
                            else{
//                                echo "-";
                            }
                            ?>
                            <td style="background-color: <?php echo $background?>">
                                <small><?php echo (isset($schedule->shift))? $schedule->shift->name:"-";?></small>
                            </td>
                        <?php } ?>
                    </tr>
                    <?php $no++;?>
                <?php } ?>
        	</tbody>
        </table><!-- end table -->
    </div><!-- box-body -->
</div><!-- box box-solid box-default -->
