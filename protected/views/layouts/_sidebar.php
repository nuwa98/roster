<ul class="sidebar-menu">
	<li class="header">MENU UTAMA</li>
	<!-- Optionally, you can add icons to the links -->

    <!-- menu for admin sahaja -->
    <?php if (Yii::app()->getModule('user')->isAdmin()){ ?>

        <!-- menu user -->
        <li class="treeview <?php echo (in_array(Yii::app()->controller->id, array('admin')))?"active":"" ?>">
            <a href="#"><i class="fa fa-user"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                <li><a href="<?php echo Yii::app()->createUrl('/user/admin/create') ?>"><i class="fa fa-plus"></i> Create</a></li>
                <li><a href="<?php echo Yii::app()->createUrl('/user/admin') ?>"><i class="fa fa-th"></i> List</a></li>
            </ul>
        </li>

        <!-- menu role management -->
        <li class=" <?php echo (in_array(Yii::app()->controller->id, array('brands')))?"active":"" ?>">
            <a href="<?php echo Yii::app()->createUrl('/rights') ?>"><i class="fa fa-desktop"></i> <span>Role Management</span></a>
        </li>

    <?php } else { ?>

        <!-- selain dari admin -->

        <!-- supervisor -->
        <?php if(Yii::app()->user->checkAccess('supervisor')) { ?>

            <!-- worker -->
            <li class=" <?php echo (in_array(Yii::app()->controller->id, array('worker')))?"active":"" ?>">
                <a href="<?php echo Yii::app()->createUrl('/worker') ?>"><i class="fa fa-users"></i> <span>Workers</span></a>
            </li>

            <!-- shift -->
            <li class=" <?php echo (in_array(Yii::app()->controller->id, array('shifts')))?"active":"" ?>">
                <a href="<?php echo Yii::app()->createUrl('/shifts') ?>"><i class="fa fa-desktop"></i> <span>Manage Shift</span></a>
            </li>

            <!-- menu venue -->
            <li class=" <?php echo (in_array(Yii::app()->controller->id, array('venue')))?"active":"" ?>">
                <a href="<?php echo Yii::app()->createUrl('/venue') ?>"><i class="fa fa-desktop"></i> <span>Manage Venue</span></a>
            </li>


            <!-- menu generate schedule -->
            <li class=" <?php echo (in_array(Yii::app()->controller->id, array('generateSchedule')))?"active":"" ?>">
                <a href="<?php echo Yii::app()->createUrl('/generateSchedule') ?>"><i class="fa fa-calendar"></i> <span>Schedule</span></a>
            </li>


        <?php } ?>

        <?php if(Yii::app()->user->checkAccess('worker')) { ?>
        <!-- worker -->
            <li class=" <?php echo (in_array(Yii::app()->controller->id, array('worker')))?"active":"" ?>">
                <a href="<?php echo Yii::app()->createUrl('/worker/report') ?>"><i class="fa fa-calendar"></i> <span>View Report</span></a>
            </li>

        <?php } ?>


        <!-- guest -->
        <?php if(Yii::app()->user->isGuest) { ?>

            <li>
                <a href="<?php echo Yii::app()->createUrl('user/login');?>">
                    <i class="fa fa-lock"></i>
                    <span>
                        Login
                    </span>
                </a>
            </li>            

        <?php } ?>
    <?php } ?>

</ul><!-- /.sidebar-menu -->