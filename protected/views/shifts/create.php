<?php
/* @var $this ShiftsController */
/* @var $model Shifts */

$this->breadcrumbs=array(
	'Shifts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Shifts', 'url'=>array('index')),
	array('label'=>'Manage Shifts', 'url'=>array('admin')),
);
?>

<h1>Create Shifts</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>