<?php
/* @var $this ShiftsController */
/* @var $model Shifts */

$this->breadcrumbs=array(
	'Shifts'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Shifts', 'url'=>array('index')),
	array('label'=>'Create Shifts', 'url'=>array('create')),
	array('label'=>'View Shifts', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Shifts', 'url'=>array('admin')),
);
?>

<h1>Update Shifts <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>