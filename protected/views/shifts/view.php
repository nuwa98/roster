<?php
/* @var $this ShiftsController */
/* @var $model Shifts */

$this->breadcrumbs=array(
	'Shifts'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Shifts', 'url'=>array('index')),
	array('label'=>'Create Shifts', 'url'=>array('create')),
	array('label'=>'Update Shifts', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Shifts', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Shifts', 'url'=>array('admin')),
);
?>

<h1>View Shifts #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'starttime',
		'endtime',
		'timestamp',
		'updated_by',
	),
)); ?>
