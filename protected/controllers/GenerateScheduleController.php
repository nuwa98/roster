<?php

class GenerateScheduleController extends Controller
{
    // testing
    public $layout='//layouts/calendar';


	public function actionIndex()
	{

        if(isset($_POST['set_month'])){
            Yii::app()->session['month'] = $_POST['month'];
            Yii::app()->session['year'] = $_POST['year'];
        }

        // set month base on session
        if(Yii::app()->session['month']){
            $month =Yii::app()->session['month']; // get month from session
        }
        else{
            $month  = date('m'); // get current month
        }

        // set year base on session
        if(Yii::app()->session['year']){
            $year =Yii::app()->session['year']; // get year from session
        }
        else{
            $year  = date('Y'); // get current year
        }

        $total_day = cal_days_in_month(CAL_GREGORIAN,$month,$year);

        // get all staf
        $workers = TblUsers::model()->with(array(
            'role'=>array(
                'condition'=>"role.itemname='worker'",
            ),
        ))->findAll();

        $shifts = Shifts::model()->findAllByAttributes(array('off'=>0));

		$this->render('index',array(
            'shifts'=>$shifts,
            'month'=>$month,
            'year'=>$year,
            'total_day'=>$total_day,
            'workers'=>$workers,
        ));
	}

    /**
     * Set zon for each day
     * @param $id
     */
    public function actionSetzon($id)
    {
        // put value $id to $day to avoid confuse variable name
        $day = $id;

        // set month base on session
        if(Yii::app()->session['month']){
            $month =Yii::app()->session['month']; // get month from session
        }
        else{
            $month  = date('m'); // get current month
        }

        // set year base on session
        if(Yii::app()->session['year']){
            $year =Yii::app()->session['year']; // get year from session
        }
        else{
            $year  = date('Y'); // get current year
        }

        // change format using php date function
        $date = date('Y-m-d', strtotime($year.'-'.$month.'-'.$day));

        $schedules = Schedules::model()->with(array(
            'shift'=>array(
                'condition'=>'off=0',
                'sort'=>array(
                    'defaultOrder'=>'id',
                )
            )
        ))->findAllByAttributes(array("date"=>$date));


        if(isset($_POST['set_zone'])){

            $post_id = $_POST['id'];
            $post_zone = $_POST['zone'];

            foreach($post_id as $k=>$v)
            {
                // get schedule by id
                $update_schedule = Schedules::model()->findByPk($v);
                $update_schedule->venue_id = $post_zone[$k];
                $update_schedule->save();

            }
            Yii::app()->user->setFlash('success', 'Zone has been set by staff');
            $this->redirect(array('setzon', 'id'=>$day));

        }

        $this->render('setzon', compact('date', 'month', 'year', 'schedules'));
    }


    /**
     * generate schedule base on month and year
     */
    public function actionGenerate()
    {
        // set month base on session
        if(Yii::app()->session['month']){
            $month =Yii::app()->session['month']; // get month from session
        }
        else{
            $month  = date('m'); // get current month
        }

        // set year base on session
        if(Yii::app()->session['year']){
            $year =Yii::app()->session['year']; // get year from session
        }
        else{
            $year  = date('Y'); // get current year
        }


        $datestring="{$year}-{$month}-30 first day of last month";
        $dt=date_create($datestring);
        $previous_month = $dt->format('m'); //2011-02
        $previous_year = $dt->format('Y'); //2011-02


        // try to get data by same month and year. If found, delete current data and insert new record
        $find_schedule = Schedules::model()->findAll(array(
            'condition'=>'MONTH(date) = :month and YEAR(date) = :year',
            'params'=>array(
                ':month'=>$month,
                ':year'=>$year,
            ),
        ));

        if(count($find_schedule) > 0)
        {
            Schedules::model()->deleteAll(array(
                'condition'=>'MONTH(date) = :month and YEAR(date) = :year',
                'params'=>array(
                    ':month'=>$month,
                    ':year'=>$year,
                ),
            ));
        }

        // get all staf
        $workers = TblUsers::model()->with(array(
            'role'=>array(
                'condition'=>"role.itemname='worker'",
            ),
        ))->findAll();

        // get all shift available yang bukan cuti
        $shifts = Shifts::model()->findAllByAttributes(array('off'=>0));

        // get all shift offday
        $offdays = Shifts::model()->findAllByAttributes(array('off'=>1));

        // get total venue
        $venue = Venue::model()->findAll();
        $total_venue = count($venue);

//
        $total_day = cal_days_in_month(CAL_GREGORIAN,$month,$year);
//        echo "<h2>There was {$total_day} days in {$month} {$year}</h2>";

        // loop worker
        $start=0;
        foreach($workers as $key => $worker)
        {
//            echo "<h2>Worker : ".$worker->id.'</h2>';
            if($key % 5 == 0){
//                echo '<hr>';
                ++$start;
            }

            ################################################################################################
            # Kebelakang balik untuk jangan biarkan ruang kosong di hadapan sebelum tarikh mula
            ################################################################################################

            $start_balance = $start - 1;
            if($start_balance>0)
            {
//                echo $start_balance.'<br>';
                $back_loop = 0;

                $loop_off = 1;
                $loop_a = 1;
                $loop_b = 1;
                $loop_c = 1;

                for($i=$start_balance; $i>0; $i--)
                {
                    $date = $year.'-'.$month.'-'.$i;

                    if($back_loop===0)
                        $back_loop = 1;

                    if($back_loop===1)
                    {
                        $done_off=false;
                        $done_c=false;
                        $done_b=false;
                        $done_a=false;
                    }

                    if(!$done_off){
//                        echo $date.' --> ';
                        $schedule = new Schedules;
                        $schedule->user_id = $worker->id;
                        $schedule->shift_id = 4; // ofday
                        $schedule->date = $date;
                        $schedule->updated_by = Yii::app()->user->id;
                        $schedule->save();
//                        echo "Shift OFF <br><br>";

                        $done_off=true;
                        $back_loop=2;
                        continue;
                        // done C, move to shift B
                    }

                    if($done_off && $done_a==false)
                    {
//                        echo $date.' --> ';
                        $schedule = new Schedules;
                        $schedule->user_id = $worker->id;
                        $schedule->shift_id = 3;
                        $schedule->date = $date;
                        $schedule->updated_by = Yii::app()->user->id;
                        $schedule->save();

//                        echo "shift A " . $loop_a . '<br>';
                        $back_loop=2;

                        if($loop_a == 2){
                            $done_a = true; // selesai shift A
                            continue;
                            // done C, move to shift B
                        }
                        $loop_a++;
//                    break;
                    } // if($done_off && $done_a==false)

                    if($done_a && $done_b==false)
                    {
//                        echo $date.' --> ';
                        $schedule = new Schedules;
                        $schedule->user_id = $worker->id;
                        $schedule->shift_id = 2;
                        $schedule->date = $date;
                        $schedule->updated_by = Yii::app()->user->id;
                        $schedule->save();

//                        echo "shift B " . $loop_b . '<br>';
                        $back_loop=2;

                        if($loop_b == 2){
                            $done_b = true; // selesai shift A
                            continue;
                            // done C, move to shift B
                        }
                        $loop_b++;
                    }
                    if($done_b && $done_c==false)
                    {
//                        echo $date.' --> ';
                        $schedule = new Schedules;
                        $schedule->user_id = $worker->id;
                        $schedule->shift_id = 1;
                        $schedule->date = $date;
                        $schedule->updated_by = Yii::app()->user->id;
                        $schedule->save();

//                        echo "shift C " . $loop_c . '<br>';
                        $back_loop=2;

                        if($loop_c == 2){
                            $back_loop=0;
                            $loop_c=1;
                            $loop_b=1;
                            $loop_a=1;
                            continue;
                            // done C, move to shift B
                        }
                        $loop_c++;
                    }
                }
            }
            ################################################################################################

            $new_load=0;
            // loop day
            $loop_c=1;
            $loop_b=1;
            $loop_a=1;
            for($i=$start; $i <= $total_day; $i++)
            {
                $date = $year.'-'.$month.'-'.$i;

                if($new_load===0)
                    $new_load = 1;

                if($new_load===1)
                {
                    $done_c=false;
                    $done_b=false;
                    $done_a=false;
                }

                ################################################################################################
                # Untuk yang kehadapan, dan seterusnya
                ################################################################################################
                if(!$done_c)
                {

                    $schedule = new Schedules;
                    $schedule->user_id = $worker->id;
                    $schedule->shift_id = 1;
                    $schedule->date = $date;
                    $schedule->updated_by = Yii::app()->user->id;
                    $schedule->save();

//                    echo $date.' --> ';
//                    echo "shift C " . $loop_c . '<br>';
                    $new_load=2;

                    if($loop_c == 2){
                        $done_c = true; // selesai shift C
                        continue;
                        // done C, move to shift B
                    }
                    $loop_c++;
//                    break;
                } // end if(!$done_c)


                // if dah selesai shift C, move to shift B pulak
                if($done_c && $done_b==false)
                {
                    $schedule = new Schedules;
                    $schedule->user_id = $worker->id;
                    $schedule->shift_id = 2;
                    $schedule->date = $date;
                    $schedule->updated_by = Yii::app()->user->id;
                    $schedule->save();
//                    echo $date.' --> ';
//                    echo "shift B " . $loop_b . '<br>';
                    $new_load=2;
                    if($loop_b==2){
                        $done_b = true;
                        continue;
                        // done C, move to shift B
                    }
                    $loop_b++;
                }

                // done shift C and B, then move to A
                if($done_b && $done_b && $done_a==false){
                    $schedule = new Schedules;
                    $schedule->user_id = $worker->id;
                    $schedule->shift_id = 3;
                    $schedule->date = $date;
                    $schedule->updated_by = Yii::app()->user->id;
                    $schedule->save();
//                    echo $date.' --> ';
//                    echo "shift A " . $loop_a . '<br>';
                    $new_load=2;
                    if($loop_a==2){
                        $done_a = true;
                        continue;
                        // done C, move to shift B
                    }
                    $loop_a++;
                }

                if($done_a){
                    $schedule = new Schedules;
                    $schedule->user_id = $worker->id;
                    $schedule->shift_id = 4; // ofday
                    $schedule->date = $date;
                    $schedule->updated_by = Yii::app()->user->id;
                    $schedule->save();
//                    echo $date.' --> ';
//                    echo "OFF <br><br>";

                    // set default value
                    $new_load=0;
                    $loop_c=1;
                    $loop_b=1;
                    $loop_a=1;
                    // done C, move to shift B
                }
                ################################################################################################
                # tamat kehadapan
                ################################################################################################

            } // end for day
//            break;

        } // end foreach worker
//        echo "Successful generate shift schedule for ".BaseFunctions::monthDetails($month);

         Yii::app()->user->setFlash('success', 'Schedule have been set up');
         $this->redirect(array('generateSchedule/index'));
    } // end function generate
}