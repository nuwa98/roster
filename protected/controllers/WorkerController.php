<?php

class WorkerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

    public function actionReport($id=null)
    {
        if($id==null)
            $id = Yii::app()->user->id;

        if(isset($_POST['set_month'])){
            Yii::app()->session['month'] = $_POST['month'];
            Yii::app()->session['year'] = $_POST['year'];
        }

        // set month base on session
        if(Yii::app()->session['month']){
            $month =Yii::app()->session['month']; // get month from session
        }
        else{
            $month  = date('m'); // get current month
        }

        // set year base on session
        if(Yii::app()->session['year']){
            $year =Yii::app()->session['year']; // get year from session
        }
        else{
            $year  = date('Y'); // get current year
        }
        $worker = TblUsers::model()->findByPk($id);

        // find working hours
        $getWorkingDay = Schedules::model()->with(array(
            'shift'=>array(
                'condition'=>'off=0',
            )
        ))->findAll(array(
            'condition'=>'MONTH(date) = :month and YEAR(date) = :year and user_id=:id',
            'params'=>array(
                ':month'=>$month,
                ':year'=>$year,
                ':id'=>$id
            ),
            'order'=>'date ASC',
        ));
        $totalWorkingDays = count($getWorkingDay);
        $working_hours = count($getWorkingDay) * 7;

        // find off day
        $offDay = Schedules::model()->with(array(
            'shift'=>array(
                'condition'=>'off=1',
            )
        ))->findAll(array(
            'condition'=>'MONTH(date) = :month and YEAR(date) = :year and user_id=:id',
            'params'=>array(
                ':month'=>$month,
                ':year'=>$year,
                ':id'=>$id
            ),
            'order'=>'date ASC',
        ));
        $totalOffDay = count($offDay);

        // try to get data by same month and year. If found, delete current data and insert new record
        $find_schedule = Schedules::model()->findAll(array(
            'condition'=>'MONTH(date) = :month and YEAR(date) = :year and user_id=:id',
            'params'=>array(
                ':month'=>$month,
                ':year'=>$year,
                ':id'=>$id
            ),
            'order'=>'date ASC',
        ));

        $this->render('report', compact('worker', 'month','year', 'find_schedule','working_hours', 'totalOffDay', 'totalWorkingDays'));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        // create new object
		$model=new TblUsers;
        $profile=new TblProfiles;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TblUsers']))
		{
            // get post data
			$model->attributes      =$_POST['TblUsers'];
			$profile->attributes    =$_POST['TblProfiles'];

            // set validation
            $valid = $model->validate();
            $valid = $profile->validate() & $valid;

            if($valid){
                try
                {
                    $transaction = $model->getDbConnection()->beginTransaction();


                    // set as no ic is default password
                    $model->password = Yii::app()->getModule('user')->encrypting($profile->nokp);
                    $model->activkey=Yii::app()->getModule('user')->encrypting(microtime().$profile->nokp);

                    $model->status = 1;

                    // save user model
                    $model->save();

                    // save user profile
                    $profile->user_id = $model->id;
                    $profile->save();

                    // add on table authassignment
                    $authorizer = Yii::app()->getModule("rights")->getAuthorizer();
                    $authorizer->authManager->assign('worker', $model->id);

                    $transaction->commit();

                    Yii::app()->user->setFlash('success', 'Data Insert!');
                    $this->redirect(array('/worker'));

                }
                catch(Exception $e)
                {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error', 'Error while inserting data!');
                    $this->redirect(array('/worker'));
                }

            }
		}

        // call view
		$this->render('create',array(
			'model'=>$model,
            'profile'=>$profile,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TblUsers']))
		{
			$model->attributes=$_POST['TblUsers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new TblUsers('worker');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['TblUsers']))
			$model->attributes=$_GET['TblUsers'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TblUsers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TblUsers::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TblUsers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tbl-users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
