<?php

class m150627_130347_create_table_schedule extends CDbMigration
{
	public function up()
	{
        $this->createTable('schedules', array(
            'id'=>'pk',
            'shift_id'=>'integer',
            'user_id'=>'integer',
            'date'=>'date',
            'timestamp'=>'timestamp',
            'updated_by'=>'integer',
        ));

        $this->addForeignKey('schedules_shift_id_foreign', 'schedules', 'shift_id', 'shifts', 'id', 'CASCADE');
        $this->addForeignKey('schedules_user_id_foreign', 'schedules', 'user_id', 'tbl_users', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('schedules_updated_by_foreign', 'schedules', 'updated_by', 'tbl_users', 'id', 'SET NULL');
	}

	public function down()
	{
		$this->dropTable('schedules');
		return false;
	}
}