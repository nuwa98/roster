<?php

class m150626_110332_create_table_venue extends CDbMigration
{
    public function up()
    {
        $this->createTable('venue', array(
            'id'=>'pk',
            'name'=>'string',
            'timestamp'=>'timestamp',
            'updated_by'=>'integer',
        ));

        $this->addForeignKey('venue_updated_by_foreign', 'venue', 'updated_by', 'tbl_users', 'id', 'SET NULL');
    }

    public function down()
    {
        $this->dropTable('venue');
        return false;
    }
}